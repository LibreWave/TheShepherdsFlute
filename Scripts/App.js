namespace App
{
	Synth.deferCallbacks(true);
	Engine.loadAudioFilesIntoPool();
	Engine.loadImageIntoPool("{PROJECT_FOLDER}Icon.png");
	Engine.setAllowDuplicateSamples(false);
}