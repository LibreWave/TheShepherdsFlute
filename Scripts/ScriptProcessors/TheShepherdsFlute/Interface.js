/*
    Copyright 2021, 2022, 2023 David Healey

    This file is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This file is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with This file. If not, see <http://www.gnu.org/licenses/>.
*/

Content.makeFrontInterface(1000, 710);

include("App.js");
include("Manifest.js");
include("Theme.js");
include("RhapsodyBoilerplate/includes/Ui.js");
include("RhapsodyBoilerplate/includes/Configuration.js");
include("RhapsodyBoilerplate/includes/LookAndFeel.js");
include("RhapsodyBoilerplate/includes/Paths.js");
include("RhapsodyBoilerplate/includes/Expansions.js");
include("RhapsodyBoilerplate/includes/Header.js");
include("RhapsodyBoilerplate/includes/Footer.js");
include("RhapsodyBoilerplate/includes/Presets.js");
include("RhapsodyBoilerplate/includes/UserSettings.js");
include("RhapsodyBoilerplate/includes/Card.js");
include("RhapsodyBoilerplate/includes/Envelope.js");
include("RhapsodyBoilerplate/includes/Patches.js");
include("RhapsodyBoilerplate/includes/Articulations.js");
include("RhapsodyBoilerplate/includes/Velocity.js");
include("RhapsodyBoilerplate/includes/ZoomHandler.js");
function onNoteOn()
{
	local n = Message.getNoteNumber();
	Articulations.onNoteOnHandler(n);
}
 function onNoteOff()
{
	
}
 function onController()
{
	local cc = Message.getControllerNumber();
	local cv = Message.getControllerValue();

	if (cc == 32 || Message.isProgramChange())
		Articulations.onControllerHandler(cv);
}
 function onTimer()
{
	
}
 function onControl(number, value)
{
	
}
 