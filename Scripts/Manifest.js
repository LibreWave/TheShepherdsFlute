/*
    Copyright 2021, 2022, 2023 David Healey

    This file is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This file is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with This file. If not, see <http://www.gnu.org/licenses/>.
*/

namespace Manifest
{
	const samplers = [
		{
			id: "Sampler0",
			properties: {VoiceAmount: 96, VoiceLimit: 64, Gain: 0, SampleMap: "sustain"}
		},
		{
			id: "Sampler1",
			properties: {VoiceAmount: 32, VoiceLimit: 16, Gain: -12, SampleMap: "transitions"}
		},
		{
			id: "Sampler2",
			properties: {VoiceAmount: 64, VoiceLimit: 32, Gain: -3, SampleMap: "articulations"}
		}
	];
	
	const components = [
		{
			id: "knbAHDSR1",
			properties: {min: 0, max: 2500, defaultValue: 2, middlePosition: 1000}
		},
		{
			id: "knbAHDSR4",
			properties: {min: -100, max: 0, defaultValue: -2}
		}
	];
	
	const modulators = [
		{
			id: "sampler2Filter0FrequencyVelocity",
			properties: {Bypass: false}
		},
		{
			id: "sampler2Filter0FrequencyCc",
			properties: {Bypass: true}
		},
		{
			id: "sampler2GainVelocity",
			properties: {Bypass: false}
		},
		{
			id: "sampler2GainCc",
			properties: {Bypass: true}
		}
	];
	
	const scripts = [
		{
			id: "retrigger",
			properties: {Bypass: false}
		}
	];

	const articulations = [
		{
			id: "Performance",
      		program: 1,
      		samplers: [0, 1, 3],
      		ahdsr: "sampler0GainAHDSR",
      		modulators: [],
			scripts: [
				{
					id: "retrigger",
					properties: {Bypass: true}
				}
			],
			components: [
				{
					id: "knbAHDSR1",
					properties: {min: 250, max: 1500, defaultValue: 400, middlePosition: 1000}
				},
				{
					id: "knbAHDSR4",
					properties: {min: -99, max: 0, defaultValue: -0.5}
				}
			]
    	},
		{
			id: "Staccato",
			program: 2,
			samplers: [2],
			ahdsr: "sampler2GainAHDSR",
			modulators: [],
			scripts:[
				{
					id: "sampler2RoundRobin",
					properties: {FirstGroup: 1, Count: 3, Borrowed: false}
				}
			],
			disabledComponents: ["knbVibratoRate", "knbVibratoDepth", "knbDynamics"]
		},
		{
			id: "Staccatissimo",
			program: 3,
			samplers: [2],
			ahdsr: "sampler2GainAHDSR",
			modulators: [
				{
					id: "sampler2Filter0FrequencyVelocity",
					properties: {Bypass: false}
				},
				{
					id: "sampler2Filter0FrequencyCc",
					properties: {Bypass: true}
				},
				{
					id: "sampler2GainVelocity",
					properties: {Bypass: false}
				},
				{
					id: "sampler2GainCc",
					properties: {Bypass: true}
				}
			],
			scripts: [
				{
					id: "sampler2RoundRobin",
					properties: {FirstGroup: 4, Count: 3, Borrowed: true}
				}
			],
			disabledComponents: ["knbVibratoRate", "knbVibratoDepth", "knbDynamics"]
		},
		{
	      	id: "Overblown Sustain",
	      	program: 4,
	      	samplers: [2],
	      	ahdsr: "sampler2GainAHDSR",
	      	modulators: [
		      	{
		      		id: "sampler2Filter0FrequencyVelocity",
		      		properties: {Bypass: true}
		      	},
	      		{
	      			id: "sampler2Filter0FrequencyCc",
	      			properties: {Bypass: false}
	      		},
	      		{
	      			id: "sampler2GainVelocity",
	      			properties: {Bypass: true}
	      		},
				{
					id: "sampler2GainCc",
					properties: {Bypass: false}
				}
	      	],
	      	scripts:[
				{
					id: "sampler2RoundRobin",
					properties: {FirstGroup: 11, Count: 1, Borrowed: true}
				}
			],
	      	disabledComponents: ["knbVibratoRate", "knbVibratoDepth"]
		},
		{
			id: "Vocal Sustain",
	      	program: 5,
	      	samplers: [2],
	      	ahdsr: "sampler2GainAHDSR",
	      	modulators: [
		      	{
		      		id: "sampler2Filter0FrequencyVelocity",
		      		properties: {Bypass: true}
		      	},
	      		{
	      			id: "sampler2Filter0FrequencyCc",
	      			properties: {Bypass: false}
	      		},
	      		{
	      			id: "sampler2GainVelocity",
	      			properties: {Bypass: true}
	      		},
				{
					id: "sampler2GainCc",
					properties: {Bypass: false}
				}
	      	],
	      	scripts:[
				{
					id: "sampler2RoundRobin",
					properties: {FirstGroup: 12, Count: 1, Borrowed: false}
				}
			],
	      	disabledComponents: ["knbVibratoRate", "knbVibratoDepth"]
		},
		{
			id: "Vocal Staccato",
			program: 6,
			samplers: [2],
			ahdsr: "sampler2GainAHDSR",
			modulators: [
				{
					id: "sampler2Filter0FrequencyVelocity",
					properties: {Bypass: false}
				},
				{
					id: "sampler2Filter0FrequencyCc",
					properties: {Bypass: true}
				},
	      		{
	      			id: "sampler2GainVelocity",
	      			properties: {Bypass: false}
	      		},
				{
					id: "sampler2GainCc",
					properties: {Bypass: true}
				}
			],
			scripts: [
				{
					id: "sampler2RoundRobin",
					properties: {FirstGroup: 13, Count: 3, Borrowed: false}
				}
			],
			disabledComponents: ["knbVibratoRate", "knbVibratoDepth", "knbDynamics"]
		},
		{
			id: "Vocal Staccatissimo",
			program: 7,
			samplers: [2],
			ahdsr: "sampler2GainAHDSR",
			modulators: [
				{
					id: "sampler2Filter0FrequencyVelocity",
					properties: {Bypass: false}
				},
				{
					id: "sampler2Filter0FrequencyCc",
					properties: {Bypass: true}
				},
	      		{
	      			id: "sampler2GainVelocity",
	      			properties: {Bypass: false}
	      		},
				{
					id: "sampler2GainCc",
					properties: {Bypass: true}
				}
			],
			scripts: [
				{
					id: "sampler2RoundRobin",
					properties: {FirstGroup: 16, Count: 3, Borrowed: false}
				}
			],
			disabledComponents: ["knbVibratoRate", "knbVibratoDepth", "knbDynamics"]
		},
		{
			id: "Finger Taps",
			program: 8,
			samplers: [2],
			ahdsr: "sampler2GainAHDSR",
			modulators: [
				{
					id: "sampler2Filter0FrequencyVelocity",
					properties: {Bypass: true}
				},
				{
					id: "sampler2Filter0FrequencyCc",
					properties: {Bypass: false}
				},
	      		{
	      			id: "sampler2GainVelocity",
	      			properties: {Bypass: false}
	      		},
				{
					id: "sampler2GainCc",
					properties: {Bypass: true}
				}
			],
			scripts: [
				{
					id: "sampler2RoundRobin",
					properties: {FirstGroup: 7, Count: 4, Borrowed: true}
				}
			],
			disabledComponents: ["knbVibratoRate", "knbVibratoDepth", "knbDynamics"]
		}
	];

	const keyColours = {
		keyswitches: 0xff550000,
		playable: 0xff3e312e,
		retrigger: 0xff000055
	};	
	
	const patches = [
	{
		id: "Default",
		index: 0,
		keyRanges: [
        	[60, 91, keyColours.playable],
        	[24, 31, keyColours.keyswitches],
        	[36, 36, keyColours.retrigger]
      	],
  		firstKs: 24,
  		articulations: {
    		active: [0, 1, 2, 3, 4, 5, 6, 7],
    		keyRanges: [
    			[
	    			[60, 91, keyColours.playable],
	    			[24, 31, keyColours.keyswitches],
	    			[36, 36, 0]
    			]
    		]
  		}
	}];
}
