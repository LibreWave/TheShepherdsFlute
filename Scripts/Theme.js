namespace Theme
{
	const style = {
		"card": 
		{
			"bg": 0xff1d1d21
		},
		"presetBrowser":
		{
			"columnBg": 0xff161619,
			"searchBarBg": 0xff161619,
			"searchBarIcon": 0xff9F9fb1,
			"iconButtonBg": Colours.white,
			"itemSelected": 0xff302f34,
			"itemHighlight": 0xff6579A0,
			"itemText": Colours.white,
		},
		"alertWindow": {
			"textButtonBg": 0xff161619,
			"textButtonTextColour": Colours.white
		}
	};
}
